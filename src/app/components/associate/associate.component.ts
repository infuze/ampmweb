import { Component, OnInit } from '@angular/core';
import { EmailService } from 'src/app/services/email.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-associate',
  templateUrl: './associate.component.html',
  styleUrls: ['./associate.component.scss']
})
export class AssociateComponent implements OnInit {

  public associateForm: FormGroup;
  public loadingSendForm: boolean;
  public files: any = [];
  public readyFile: any;

  constructor(
    private emailService: EmailService
  ) { }

  ngOnInit() {
    this.initForm();
  }


  addFile(event) {
    let target = event.target || event.srcElement;
    this.files.push(target.files);
  }

  sendEmail() {
    let filelist: FileList = this.files;
    const formData = new FormData();
    for (let i = 0; i < filelist.length; i++) {
      this.readyFile = filelist[i];
      formData.append('attachment', this.readyFile[0]);
    }

    Object.keys(this.associateForm.value).forEach((key) => {
      formData.append(key, this.associateForm.get(key).value);
    })

    this.loadingSendForm = true;
    this.emailService.sendEmail(formData, 'asociate').subscribe(res => {
      console.log('AppComponent Success', res);
    }, error => {
      console.log('AppComponent Error', error);
    });
  }

  initForm() {
    this.associateForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      dni: new FormControl('', [Validators.required]),
      tel: new FormControl('', [Validators.required]),
      cuil: new FormControl(''),
      domicilio: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      localidad: new FormControl('', [Validators.required]),
      provincia: new FormControl('', [Validators.required]),
      estadoCivil: new FormControl('', [Validators.required]),
      message: new FormControl(''),
      dniFile: new FormControl(''),
    });
  }

}
