import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as AOS from 'aos';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  constructor(
    private router: Router,
    public ngxSmartModalService: NgxSmartModalService
  ) {}

  getUnidad() {
    const chain = this.router.url.split('/'); // Check url content after '/'
    const validChain = chain[1].indexOf('#') !== -1; // boolean value - chain contains '#'
    const subChain = chain[1].split('#'); // Check url content after '#'
    if (validChain) {
      const el = document.getElementById(subChain[1]);
      el.scrollIntoView(true);
    }
  }

  ngOnInit() {
    AOS.init();
  }
  
  ngAfterViewInit() {
    const instance = this;
    setTimeout(function () {
      instance.getUnidad();
    }, 100);
  }

}
