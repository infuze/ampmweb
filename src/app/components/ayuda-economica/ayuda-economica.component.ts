import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import * as AOS from 'aos';

@Component({
  selector: 'app-ayuda-economica',
  templateUrl: './ayuda-economica.component.html',
  styleUrls: ['./ayuda-economica.component.scss']
})
export class AyudaEconomicaComponent implements OnInit {

  navScrolled = true;

  constructor(private router: Router) { }

  ayudaInfo = [
    {
      'title': 'Tus objetivos:',
      'icon': 'objetivos.svg',
      'items': [
        'Compra de Vehículos.',
        'Refacción de viviendas y/o comercios.',
        'Viajes.',
        'Cancelación de saldos.',
        'Capital de trabajo',
        'Capacitación laboral y/o educacional.'
      ]
    },
    {
      'title': 'Mínimos requisitos:',
      'icon': 'requisitos.svg',
      'items': [
        'Trabajador Activo.',
        'Recibo de sueldo de  6 meses de antigüedad.',
        'Edad: 21 años.'
      ]
    },
    {
      'title': 'Ventajas:',
      'icon': 'ventajas.svg',
      'items': [
        'Tasa Fija y en pesos.',
        'Aprobación aprox 24hs.',
        'Mínimo de cuotas 6 y máximo 18.',
        'Sistema Francés.',
        'Relación cuota ingreso 30%.'
      ]
    },
  ];

  ngOnInit() {
    AOS.init();

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }

}
