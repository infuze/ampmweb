import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AyudaEconomicaComponent } from './ayuda-economica.component';

describe('AyudaEconomicaComponent', () => {
  let component: AyudaEconomicaComponent;
  let fixture: ComponentFixture<AyudaEconomicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AyudaEconomicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AyudaEconomicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
