import { Component, OnInit, Input } from '@angular/core';
import { ScrollToService } from 'ng2-scroll-to-el';
declare var M: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('status') navScrolled: boolean;

  public instances: any;

  constructor() {
  }

  // When the user scrolls down 20px from the top of the document, slide down the navbar
  // When the user scrolls to the top of the page, slide up the navbar (50px out of the top view)

  scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById('navbar').classList.add('scrolled');
    } else if (this.navScrolled !== true) {
      document.getElementById('navbar').classList.remove('scrolled');
    }
  }

  ngOnInit() {
    const options = {};
    document.addEventListener('DOMContentLoaded', () => {
      const sidenav = document.querySelectorAll('.sidenav');
      if (!this.instances) {
        this.instances = M.Sidenav.init(sidenav, options);
      }
    });
    console.log('navbar');
    

    window.onscroll = () => this.scrollFunction();
  }

}
