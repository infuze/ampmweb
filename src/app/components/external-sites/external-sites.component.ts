import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-external-sites',
  templateUrl: './external-sites.component.html',
  styleUrls: ['./external-sites.component.scss']
})
export class ExternalSitesComponent implements OnInit {

  externalSites = [
    {
      title: 'Home Banking',
      url: 'https://www.inshome.com.ar/ampm',
      icon: 'bank.svg'
    },
    {
      title: 'Turismo',
      url: 'https://ampm.grupo-america.com/',
      icon: 'flight.svg'
    },
    {
      title: 'E-Commerce',
      url: 'https://ampm.inssales.com.ar/sale/principal.php',
      icon: 'online-shopping.svg'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
