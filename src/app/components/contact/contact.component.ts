import { Component, OnInit } from '@angular/core';

declare var google: any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor() { }

  // Initialize and add the map
  initMap() {
    // The location of Uluru
    const ampm = { lat: -32.9458179, lng: -60.64191 };
    // The map, centered at Uluru
    const map = new google.maps.Map(
      document.getElementById('map'), { zoom: 16, center: ampm });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({ position: ampm, map: map });
  }

  ngOnInit() {
    this.initMap();
  }

}
