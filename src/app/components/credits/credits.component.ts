import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.scss']
})
export class CreditsComponent implements OnInit {

  private albums: Array<any> = [];

  swiperTourism: SwiperConfigInterface = {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: false,
    loop: true,
    initialSlide: 0,
    effect: 'fade',
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    }
  };

  tourismItems = [
    {
      title: 'Turismo',
      subtitle: '¡Inauguramos el Departamento de Turismo! Estamos para ayudarte y brindarte el mejor asesoramiento.\nSi tenés ganas de viajar, consultanos!',
      text: 'Asoc. Mutual Pedro de Mendoza. Leg 18033',
      text2: '+549 341 5042003',
      text3: 'turismo@ampm.com.ar',
      // blockInfo: '#QuedateEnCasa',
      background: '2.jpg',
      image: ''
    },
    // {
    //   title: 'Brasil 2020',
    //   subtitle: 'Desde USD 690',
    //   text: 'Próximas salidas en Abril',
    //   blockInfo: '7 y 8 noches',
    //   background: 'brasil.jpg',
    //   image: 'brasil-info.jpeg'
    // },
    // {
    //   title: 'NOA con Bolivia',
    //   subtitle: 'Desde $25.999',
    //   text: 'Salida 5 de Abril',
    //   blockInfo: '7 noches',
    //   background: 'noa.jpg',
    //   image: 'noa-info.jpg'
    // },
    // {
    //   title: 'Mar del Plata',
    //   subtitle: 'Desde $11.399',
    //   text: 'Salida 8 de Abril',
    //   blockInfo: '3 noches',
    //   background: '1.jpg',
    //   image: '1-info.jpg',
    // },
    // {
    //   title: 'Merlo',
    //   subtitle: 'Desde $9.866',
    //   text: 'Salida 8 de Abril',
    //   blockInfo: '3 noches',
    //   background: 'merlo.jpg',
    //   image: 'merlo-info.jpg'
    // },
    // {
    //   title: 'Bariloche aéreo',
    //   subtitle: 'Desde $25.799',
    //   text: 'Salida 9 de Abril',
    //   blockInfo: '5 noches',
    //   background: 'bariloche.jpg',
    //   image: 'bariloche-info.jpg'
    // },
    // {
    //   title: 'Talampaya',
    //   subtitle: 'Desde $9.999',
    //   text: 'Salida 8 de Abril',
    //   blockInfo: '3 noches',
    //   background: 'talampaya.jpg',
    //   image: 'talampaya-info.jpg'
    // },
  ];

  constructor(
    private lightbox: Lightbox
  ) { }

  ngOnInit() {
    this.initAlbum();
  }

  initAlbum() {
    this.tourismItems.forEach(element => {
      const src = 'assets/tourism/' + element.image;
      const album = {
        src: src
      }
      this.albums.push(album);
    });
  }

  open(index: number): void {
    // open lightbox
    this.lightbox.open(this.albums, index);
  }

  close(): void {
    // close lightbox programmatically
    this.lightbox.close();
  }

}
