import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { EmailService } from '../../../services/email.service';

@Component({
  selector: 'app-form-house',
  templateUrl: './form-house.component.html',
  styleUrls: ['./form-house.component.scss']
})
export class FormHouseComponent implements OnInit {

  public houseForm: FormGroup;
  public loadingSendForm = false;
  
  constructor(
    private emailService: EmailService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.houseForm = new FormGroup({
      location: new FormControl('', [Validators.required]),
      value: new FormControl('', [Validators.required]),
      floor: new FormControl(''),
      hasAlarm: new FormControl(''),
      permanentPlace: new FormControl('', [Validators.required]),
      typeOfPlace: new FormControl('', [Validators.required]),
      hasGrille: new FormControl(''),
      contact: new FormControl('', [Validators.required]),
      tel: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  sendForm() {
    this.loadingSendForm = true;
    this.emailService.sendEmail(this.houseForm.value, 'hogar').subscribe(
      (res) => {
        this.loadingSendForm = false;
      },
      (err) => {
        this.loadingSendForm = false;
      }
    );
  }

}
