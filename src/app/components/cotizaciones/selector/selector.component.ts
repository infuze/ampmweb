import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-selector-cotizaciones',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit {

  @Output() selectedTab = new EventEmitter<string>();
  @Input('active') activeTab;
  public selectedTabTitle: string;

  public cotizaciones = [
    {
      title: 'art',
      icon: 'engineer'
    },
    {
      title: 'auto',
      icon: 'car'
    },
    {
      title: 'hogar',
      icon: 'house'
    }
  ];

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {
    this.selectedTabTitle = this.activeTab;
  }

  goToForm(title) {
    this.router.navigate([`/cotizaciones/${title}`]);
    this.selectedTabTitle = title;
    this.selectedTab.emit(title);
  }

}
