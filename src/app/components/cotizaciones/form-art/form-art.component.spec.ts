import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormArtComponent } from './form-art.component';

describe('FormArtComponent', () => {
  let component: FormArtComponent;
  let fixture: ComponentFixture<FormArtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormArtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormArtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
