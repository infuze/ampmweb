import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmailService } from '../../../services/email.service';

@Component({
  selector: 'app-form-art',
  templateUrl: './form-art.component.html',
  styleUrls: ['./form-art.component.scss']
})
export class FormArtComponent implements OnInit {

  @ViewChild('artFormChild') artFormChild;

  public artForm: FormGroup;
  public loadingSendForm: boolean;
  public files: any = [];
  public readyFile: any;

  public items = [
    'Buenos Aires',
    'Catamarca',
    'Chaco',
    'Chubut',
    'Ciudad Autónoma de Buenos Aires',
    'Córdoba',
    'Corrientes',
    'Entre Ríos',
    'Formosa',
    'Jujuy',
    'La Pampa',
    'La Rioja',
    'Mendoza',
    'Misiones',
    'Neuquén',
    'Río Negro',
    'Salta',
    'San Juan',
    'Santa Cruz',
    'Santa Fe',
    'Santiago del Estero',
    'Tierra del Fuego',
    'Tucumán'
  ]

  constructor(
    public emailService: EmailService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.artForm = new FormGroup({
      businessName: new FormControl('', [Validators.required]),
      cuit: new FormControl('', [Validators.required]),
      contact: new FormControl('', [Validators.required]),
      tel: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      zipCode: new FormControl('', [Validators.required]),
      province: new FormControl('', [Validators.required]),
      activity: new FormControl('', [Validators.required]),
      salaryMass: new FormControl('', [Validators.required]),
      employeesNumber: new FormControl('', [Validators.required]),
      variableFee: new FormControl('', [Validators.required]),
      fixedFee: new FormControl('', [Validators.required]),
      ART: new FormControl('', [Validators.required]),
      why: new FormControl('', [Validators.required]),
      file931: new FormControl('', [Validators.required]),
    });
  }

  addFile(event) {
    let target = event.target || event.srcElement;
    this.files.push(target.files);
  }

  sendForm() {

    let filelist: FileList = this.files;
    const formData = new FormData();
    for (let i = 0; i < filelist.length; i++) {
      this.readyFile = filelist[i];
      formData.append('attachment', this.readyFile[0]);
    }

    Object.keys(this.artForm.value).forEach((key) => { 
      formData.append(key, this.artForm.get(key).value);
    })
    
    this.loadingSendForm = true;
    
    this.emailService.sendEmail(formData, 'art').subscribe(
      (res) => {
        this.loadingSendForm = false;
      },
      (err) => {
        this.loadingSendForm = false;
      }
    );
  }

}
