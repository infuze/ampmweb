import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmailService } from '../../../services/email.service';

@Component({
  selector: 'app-form-car',
  templateUrl: './form-car.component.html',
  styleUrls: ['./form-car.component.scss']
})
export class FormCarComponent implements OnInit {

  public carForm: FormGroup;
  public loadingSendForm = false;

  public fuelTypes = [
    {
      viewValue: 'Nafta'
    },
    {
      viewValue: 'Diesel'
    },
    {
      viewValue: 'Gas'
    }
  ]

  constructor(
    private emailService: EmailService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.carForm = new FormGroup({
      brand: new FormControl('', [Validators.required]),
      model: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required]),
      fuel: new FormControl('', [Validators.required]),
      fullName: new FormControl('', [Validators.required]),
      location: new FormControl('', [Validators.required]),
      coverage: new FormControl(''),
      dni: new FormControl('', [Validators.required]),
      contactTime: new FormControl(''),
      tel: new FormControl(''),
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  sendForm() {
    this.loadingSendForm = true;
    this.emailService.sendEmail(this.carForm.value, 'auto').subscribe(
      (res) => {
        this.loadingSendForm = false;
      },
      (err) => {
        this.loadingSendForm = false;
      }
    );
  }

}
