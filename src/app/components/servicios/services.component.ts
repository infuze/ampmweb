import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  constructor() { }

  services = [
    {
      'id': 'serv-medicos',
      'title': 'Servicios médicos',
      'text': 'Servicio de emergencias médicas y médicos a domicilio contando con ambulancias de alta complejidad, a través de Grupo Previnca.',
      'icon': 'doctor.svg',
      'info': [
        'Emergencias médicas',
        'Médicos a domicilio',
        'Bioquímica',
        'Alergista',
        'Enfermería',
        'Fisiatría y Kinesiología',
        'Odontología',
        'Psicología, Psicopedagogía y Psiquiatría',
      ]
    },
    {
      'id': 'asesoria-prof',
      'title': 'Asesoría profesional',
      'text': 'Brindamos servicio de asesoría profesional para el mayor crecimiento de nuestros asociados.',
      'icon': 'asesoria.svg',
      'info': [
        'Jurídica',
        'Notarial',
        'Previsional',
        'Contable',
        'Impositiva',
        'Registral'
      ]
    },
    {
      'id': 'sepelio',
      'title': 'Servicio de sepelio',
      'text': ' Contamos con el apoyo de Caramuto Rosario SA para atender a nuestros asociados en aquellos momentos difíciles.',
      'icon': 'burial.svg',
      'info': [
        'Complejo velatorio Caramuto Rosario',
        'Ataúd',
        'Carroza Fúnebre',
        'Traslados',
        'Asistencia en trámites',
        'Parcela'
      ]
    },
    {
      'id': 'universal-as',
      'title': 'Universal Assistance',
      'text': 'Contamos con asistencia al viajero, cobertura nacional e internacional.',
      'icon': 'assistance.svg',
      'info': [
        'Internaciones',
        'Traslados sanitarios',
        'Medicamentos',
        'Gastos por hotel',
        'Convalecencias',
        'Otros servicios'
      ]
    },
    {
      'id': 'lentes',
      'title': 'Provisión de lentes',
      'text': ' Provisión de un par de lentes recetados (marcos y cristales) por persona y por año, en Rosario Visión Óptica.',
      'icon': 'lentes.svg'
    },
    {
      'id': 'farmacias',
      'title': 'Descuento del 50% en farmacias',
      'text': '50% de descuento en farmacias de la Provincia, según vademécum con más de 3000 medicamentos, no genéricos.',
      'icon': 'farmacia.svg'
    },
    {
      'id': 'seguros',
      'title': 'Seguros',
      'text': 'Respondemos todas las necesidades de cobertura de riesgo de nuestros asociados. Trabajamos con más de 20 aseguradoras reconocidas.',
      'icon': 'protect.svg',
      'info': [
        'Seguros patrimoniales',
        'De incendio',
        'De vida',
        'Automotores',
        'Motocicletas, bicicletas, embarcaciones',
        'Accidentes de trabajo',
        'Profesionales',
        'Empresas comerciales e industriales'
      ]
    },
    {
      'id': 'turismo',
      'title': 'Turismo',
      'text': 'Brindamos un trato personalizado y ofrecemos la mejor opción para sus vacaciones o para concurrir a congresos, eventos o escapadas de fin de semana',
      'icon': 'safe-flight.svg'
    },
  ];


  ngOnInit() {
  }
}
