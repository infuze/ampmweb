import { Component, OnInit } from '@angular/core';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
declare var M: any;

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.scss']
})

export class BenefitsComponent implements OnInit {

  swiperConfig = {
    slidesPerView: 4,
    initialSlide: 3,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      dynamicBullets: true
    },
    effect: 'coverflow',
    centeredSlides: true,
    grabCursor: true,
    loop: true,
    coverflowEffect: {
      rotate: 10,
      stretch: 0,
      depth: 130,
      modifier: 1,
      slideShadows: false
    },
    // Responsive breakpoints
    breakpoints: {
      // when window width is <= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 0,
        coverflowEffect: {
          depth: 100
        }
      },
      // when window width is <= 480px
      480: {
        slidesPerView: 2,
        coverflowEffect: {
          depth: 100,
          stretch: 20
        }
      },
      // when window width is <= 640px
      640: {
        slidesPerView: 2,
        coverflowEffect: {
          depth: 100,
          stretch: 20
        }
      },
      768: {
        slidesPerView: 2,
        coverflowEffect: {
          depth: 100,
          stretch: 20
        }
      },
      1440: {
        slidesPerView: 4,
        spaceBetween: 10,
      },
      1920: {
        slidesPerView: 5,
        spaceBetween: 10,
      }
    }
  };


  benefits = [
    {
      'href': '',
      'img': 'assets/benefits/instituto-cam.jpeg',
      'name': 'Instituto Cam',
      'benefit': 'Hasta el 20% de descuento',
      'info': [
        'Domicilio: Santa fe 2885',
        'Tel: (341) 438-0920'
      ],
    },
    {
      'href': '',
      'img': 'assets/benefits/previnca-salud.png',
      'name': 'Previnca Salud',
      'benefit': 'Emergencias y médico a domicilio (24 hs)',
      'info': [
        'Dir: Entre Ríos 537',
        'Tel: 426-2201 / 489-4000',
      ],
    },
    {
      'href': '',
      'img': 'assets/benefits/caramuto.jpg',
      'name': 'Servicios Caramuto',
      'benefit': '50% bonificado del servicio completo',
      'info': [
        'Dir: Córdoba 2936',
        'Tel: 437-3838',
      ],
    },
    {
      'href': '',
      'img': 'assets/benefits/rosario_vision.jpg',
      'name': 'Rosario Visión',
      'benefit': 'Un par de anteojos recetados por año',
      'info': [
        'Urquiza 3126, Tel: 436-0580',
        'San Luis 2566, Tel: 421-2067',
      ],
    },
    {
      'href': '',
      'img': 'assets/benefits/univ-assistance.png',
      'name': 'Universal Assistance',
      'benefit': 'Cobertura Nacional, internacional y países limítrofes',
      'info': [
        'Argentina: 0800-999-6400',
        'Exterior: 011 4323-7777 / 011 4323-7700',
      ],
    },
    {
      'href': '',
      'img': 'assets/benefits/farmacia.jpg',
      'name': 'Descuentos en farmacias',
      'benefit': 'Hasta 50% en medicamentos no genéricos',
      'info': [
        'El vademécum se encuentra en Previnca Salud',
        'Dir: Entre Ríos 537',
        'Tel: 489-4000',
      ],
    },
    {
      'href': '',
      'img': 'assets/benefits/CERO-OCHO.png',
      'name': 'Gestoría 08',
      'benefit': 'Hasta el 25% de descuento',
      'info': [
        'Hipólito Irigoyen 838',
        'Tel: (341) 476-0224'
      ],
    },
    {
      'href': '',
      'img': 'assets/benefits/audacia-logo.jpg',
      'name': 'Audacia Moda & Fitness',
      'benefit': 'Hasta el 25% de descuento',
      'info': [
        'Dir: Italia 1352 - Local B',
        'Tel: (341) 680-1190'
      ],
    },
    
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
