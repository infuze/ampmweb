import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { AyudaEconomicaComponent } from './components/ayuda-economica/ayuda-economica.component';
import { CotizacionesComponent } from './components/cotizaciones/cotizaciones.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'ayuda-economica', component: AyudaEconomicaComponent, pathMatch: 'full' },
  { path: 'cotizaciones/:id', component: CotizacionesComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', useHash: true })
    // RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
