import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ScrollToModule } from 'ng2-scroll-to-el';

import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';

import { HomeComponent } from './components/home/home.component';

import { BannerTopComponent } from './components/banner-top/banner-top.component';
import { AboutComponent } from './components/about/about.component';
import { ServicesComponent } from './components/servicios/services.component';
import { CreditsComponent } from './components/credits/credits.component';
import { BenefitsComponent } from './components/benefits/benefits.component';
import { AssociateComponent } from './components/associate/associate.component';
import { ContactComponent } from './components/contact/contact.component';
import { ExternalSitesComponent } from './components/external-sites/external-sites.component';

// Cotizaciones
import { CotizacionesComponent } from './components/cotizaciones/cotizaciones.component';
import { SelectorComponent } from './components/cotizaciones/selector/selector.component';
import { FormArtComponent } from './components/cotizaciones/form-art/form-art.component';
import { FormCarComponent } from './components/cotizaciones/form-car/form-car.component';
import { FormHouseComponent } from './components/cotizaciones/form-house/form-house.component';

import { AyudaEconomicaComponent } from './components/ayuda-economica/ayuda-economica.component';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

import { NgxSelectModule } from 'ngx-select-ex';
import { LightboxModule } from 'ngx-lightbox';
import { NgxSmartModalModule } from 'ngx-smart-modal';

import { EmailService } from './services/email.service';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  loop: true,
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    BannerTopComponent,
    AboutComponent,
    CreditsComponent,
    BenefitsComponent,
    AssociateComponent,
    ContactComponent,
    ServicesComponent,
    AyudaEconomicaComponent,
    CotizacionesComponent,
    SelectorComponent,
    FormArtComponent,
    FormCarComponent,
    FormHouseComponent,
    ExternalSitesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ScrollToModule.forRoot(),
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    SwiperModule,
    NgxSelectModule,
    LightboxModule,
    NgxSmartModalModule.forRoot()
  ],
  providers: [
    EmailService,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
