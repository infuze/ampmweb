import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Resolve } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EmailService {
  private asociateUrl = 'php/email.php';
  private artUrl = 'php/art.php';
  private autoUrl = 'php/auto.php';
  private hogarUrl = 'php/hogar.php';

  public urlToSend: string;

  constructor (
    private http: Http
  ) {}

  sendEmail(message: any, id?: string): Observable<any> | any {
    switch (id) {
      case 'auto':
        this.urlToSend = this.autoUrl;
        break;
      case 'art':
        this.urlToSend = this.artUrl;
        break;
      case 'hogar':
        this.urlToSend = this.hogarUrl;
        break;
      case 'asociate':
        this.urlToSend = this.asociateUrl;
        break;
    }

    return this.http.post(this.urlToSend, message)
      .map(response => {
        console.log('Sending email was successfull', response);
        alert('Gracias por enviar sus datos, nos pondremos en contacto a la brevedad.');
        window.location.reload();
        return response;
      })
      .catch(error => {
        console.log('Sending email got error', error);
        return Observable.throw(error);
      });
  }
}
