<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';

$errors = '';
if(empty($errors))
{
	$postdata = file_get_contents("php://input");
	$request = json_encode($_POST);

	$businessName = $_POST['businessName'];
	$cuit = $_POST['cuit'];
	$contact = $_POST['contact'];
	$tel = $_POST['tel'];
	$from_email = $_POST['email'];
	$zipCode = $_POST['zipCode'];
	$province = $_POST['province'];
	$activity = $_POST['activity'];
	$salaryMass = $_POST['salaryMass'];
	$employeesNumber = $_POST['employeesNumber'];
	$variableFee = $_POST['variableFee'];
	$fixedFee = $_POST['fixedFee'];
	$ART = $_POST['ART'];
	$why = $_POST['why'];
	$file = $_POST['file931'];
	$to_email = 'mutualmendoza@gmail.com';
	// $to_email .= 'aguchaves@gmail.com';
	
	// // PHP MAILER
	if(isset($_FILES['attachment'])){
		$file_name = $_FILES['attachment']['name'];
		$file_size = $_FILES['attachment']['size'];
		$file_tmp = $_FILES['attachment']['tmp_name'];
		$file_type = $_FILES['attachment']['type'];
		move_uploaded_file($file_tmp, "files/".$file_name);
	}

	echo json_encode($request);

$mail = new PHPMailer();

// Activo condificacción utf-8
$mail->CharSet = 'UTF-8';
$mail->AddAttachment("files/".$file_name);
$mail->From = $from_email;
$mail->FromName = 'AMPM';
$mail->addCC($to_email);
//Content
$mail->isHTML(true); // Set email format to HTML
$mail->Subject = "Nuevo formulario de cotización de ART";
	
$contact = '
			<div style="color:#333;font-weight:300;">
									<div style="padding:30px; background-color:#1667A8; color:#fff;">
										<h2 style="color:#fff; font-weight: 300; width:auto">Web AMPM</h2>
										<hr>
										<h3 style="font-weight: 300; color: #fff;">Se ha completado el formulario de cotización de ART.</h3>
										<h4 style="font-weight: 300; color: #fff;">Información recibida:</h4>
									</div>
									<div style="background-color:#fff;color:#333;padding:30px">
										<p><strong>Razón Social: </strong>'.$businessName.'</p>
										<p><strong>CUIT: </strong>'.$cuit.'</p>
										<p><strong>Contacto: </strong>'.$contact.'</p>
										<p><strong>Tel: </strong>'. $tel.'</p>
										<p><strong>Email: </strong>'.$from_email.'</p>
										<p><strong>CP / Localidad: </strong>'.$zipCode.'</p>
										<p><strong>Provincia: </strong>'.$province.'</p>
										<p><strong>Actividad de la empresa: </strong>'.$activity.'</p>
										<p><strong>Masa salarial último mes: </strong>'.$salaryMass.'</p>
										<p><strong>Cantidad de empleados: </strong>'. $employeesNumber.'</p>
										<p><strong>Cuota variable actual (%): </strong>'. $variableFee.'</p>
										<p><strong>Cuota fija actual ($): </strong>'. $fixedFee.'</p>
										<p><strong>ART actual: </strong>'. $ART.'</p>
										<p><strong>¿Por qué desea cambiar?: </strong>'. $why.'</p>
										
									</div>
									<div style="background:#eaeaea;padding:30px;color:#333;">
										<h2 style="text-align:right;font-weight:300;color:#1667A8">Asociación Mutual Pedro de Mendoza</h2>
										<p style="text-align:left;margin-left:20px"><a style="color:#333;" href="http://ampm.com.ar">ampm.com.ar</a></p>
									</div>
			</div>
									
			';

$mail->Body = $contact;

if(!$mail->send()) {
    echo 'No se pudo enviar el archivo recargue e intente nuevamente.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}

	$response_array['status'] = 'success';
	$response_array['from'] = $from_email;
	echo json_encode($response_array);
	echo json_encode($from_email);
	header($response_array);
	return $from_email;
} else {
	$response_array['status'] = 'error';
	echo json_encode($response_array);
	header('Location: /error.html');
}




?>
