<?php
header('Content-type: application/json');
$errors = '';
if(empty($errors))
{
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);

	$brand = $request->brand;
	$model = $request->model;
	$year = $request->year;
	$fuel = $request->fuel;
	$from_name = $request->fullName;
	$location = $request->location;
	$coverage = $request->coverage;
	$dni = $request->dni;
	$contactTime = $request->contactTime;
	$tel = $request->tel;
	$from_email = $request->email;
	$to_email = 'mutualmendoza@gmail.com';
	// $to_email .= 'aguchaves@gmail.com';
	$contact = '
				<div style="color:#333;font-weight:300;">
                    <div style="padding:30px; background-color:#1667A8; color:#fff;">
                      <h2 style="color:#fff; font-weight: 300; width:auto">Web AMPM</h2>
                      <hr>
                      <h3 style="font-weight: 300; color: #fff;">Se ha completado el formulario de cotización de AUTO.</h3>
											<h4 style="font-weight: 300; color: #fff;">Información recibida:</h4>
										</div>
										<div style="background-color:#fff;color:#333;padding:30px">
											<p><strong>Marca: </strong>'.$brand.'</p>
											<p><strong>Modelo: </strong>'.$model.'</p>
											<p><strong>Año: </strong>'.$year.'</p>
											<p><strong>Tipo de combustible: </strong>'.$fuel.'</p>
											<p><strong>Nombre y apellido: </strong>'.$from_name.'</p>
											<p><strong>DNI: </strong>'. $dni.'</p>
											<p><strong>Localidad: </strong>'.$location.'</p>
											<p><strong>Cobertura: </strong>'.$coverage.'</p>
											<p><strong>Horario de Contacto: </strong>'.$contactTime.'</p>
											<p><strong>Tel: </strong>'. $tel.'</p>
											<p><strong>Email: </strong>'.$from_email.'</p>
										</div>
										<div style="background:#eaeaea;padding:30px;color:#333;">
											<h2 style="text-align:right;font-weight:300;color:#1667A8">Asociación Mutual Pedro de Mendoza</h2>
                      <p style="text-align:left;margin-left:20px"><a style="color:#333;" href="http://ampm.com.ar">ampm.com.ar</a></p>
                    </div>
				</div>
										
				';
	$website = 'AMPM';
	$email_subject = "Nuevo formulario de cotización de AUTO";
	$email_body = '<html><body>';
	$email_body .= "$contact";
	$email_body .= '</body></html>';
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$headers .= "From: $from_email\n";
	$headers .= "Reply-To: $from_email";
  $result = mail($to_email,$email_subject,$email_body,$headers);

  if(!$result) {
    var_dump("error");
    echo "Error";
  } else {
      var_dump("Success");
      echo "Success";
    }
	$response_array['status'] = 'success';
	$response_array['from'] = $from_email;
	echo json_encode($response_array);
	echo json_encode($from_email);
	header($response_array);
	return $from_email;
} else {
	$response_array['status'] = 'error';
	echo json_encode($response_array);
	header('Location: /error.html');
}




?>
